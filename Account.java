package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JPasswordField;

public class Account {

	private JFrame frame;
	private JTextField txtAdmin;
	private JPasswordField passwordField;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Account window = new Account();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Account() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 434, 261);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		txtAdmin = new JTextField();
		txtAdmin.setText("                  ADMIN");
		txtAdmin.setBounds(130, 11, 164, 20);
		panel.add(txtAdmin);
		txtAdmin.setColumns(10);
		
		JFormattedTextField formattedTextField = new JFormattedTextField();
		formattedTextField.setBounds(163, 127, 110, 20);
		panel.add(formattedTextField);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(163, 162, 110, 20);
		panel.add(passwordField);
		
		lblNewLabel = new JLabel("Email");
		lblNewLabel.setBounds(83, 130, 46, 14);
		panel.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("Password");
		lblNewLabel_1.setBounds(83, 165, 58, 14);
		panel.add(lblNewLabel_1);
	}
}
